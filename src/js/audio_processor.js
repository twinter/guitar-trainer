// Guitar trainer. A program to train navigation on a guitar.
//   Copyright (C) 2017  Tom Winter
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

'use strict'

import math from 'mathjs'
import FileSaver from 'file-saver'

export default class AudioProcessor {
  constructor () {
    // TODO: show a clipping warning if the fft is at -30dB (or the this.analyser.maxDecibels we've set)

    // create audio processing objects
    this.input = null
    this.context = new AudioContext()
    this.analyser = this.context.createAnalyser()
    this.analyser.fftSize = 4096
    this.notes = ['c', 'c#', 'd', 'd#', 'e', 'f', 'f#', 'g', 'g#', 'a', 'a#', 'b']

    // ----- SETTINGS FOR BETA-CONVERGENCE (see this.get_note for details)
    this.beta = 1.5

    this.max_iterations = 100

    this.error_threshold = .999  // similarity to the last error

    // ----- MISC SETTINGS
    // the number of the strings on the guitar
    this.string_count = 6

    // the frequency of A4 (normally at 440Hz)
    this.a4_frequency = 440

    // minimum RMS of the signal to start detection of notes
    this.min_rms = 0.005

    // minimum strength for a note to be recognised
    this.note_threshold = 0.025

    // additional threshold for every note except for the strongest
    this.note_threshold_secondary = 0.5

    // how long (in milliseconds) do we need to see the same notes to return it as a result?
    this.note_smoothing_window_length = 300

    // the loudness in dB the FFT recordings will be normalised to. Changing this will require recordings with the new settings.
    this.fft_recorder_norm_peak = -35
    // ----- END OF SETTINGS

    // bandwidth of one band in the FFT
    this.fft_bandwith = this.context.sampleRate / 2 / this.analyser.frequencyBinCount

    // topmost band in the fft to use in note detection
    this.fft_cutoff_band = this._freqToBand(6000)

    // the matrix for the fft recordings
    this.fft_recordings = null

    // the parameters included in the exported fft recordings an checked against local parameters on import
    this.fft_export_mapping = [
      ['version', 4],
      ['samplerate', this.context.sampleRate],
      ['a4_frequency', this.a4_frequency],
      ['fft_sample_size', this.analyser.fftSize],
      ['fft_norm_peak', this.fft_recorder_norm_peak],
      ['fft_cutoff_band', this.fft_cutoff_band],
    ]

    // the buffer used to store the samples from the input
    this.buffer = new Float32Array(this.analyser.fftSize)

    // allow the buffer to be up to half the buffered samples old when using a lazy update (e.g. in _updateBuffer() )
    this.buffer_decay = 1. / this.context.sampleRate * this.analyser.fftSize / 2.

    // history of notes used for smoothing the result of getNote()
    this._last_notes = []

    // the last value of h (in this.getComposition() )
    this._last_h = null

    // the last key used in this.getComposition()
    this._last_key = null

    // settings end here
    // the frequency table
    this.frequencies = new Map()

    this._buildFrequencyTable('e', 2, 'e', 5, false)  // the lowest supported note (44.8kHz, 2048 samples) is g1

    // request mic access and connect everything
    function _connectInput (stream) {
      // init audio processing
      this.input = this.context.createMediaStreamSource(stream)
      this.input.connect(this.analyser)
      console.log('connected to audio input')
    }

    _connectInput = _connectInput.bind(this)
    navigator.mediaDevices.getUserMedia({audio: true})
      .then(_connectInput)
      .catch(function (err) {
        // mic access denied
        console.log('user denied access to the audio input')
        console.error(err)
      })
  }

  get available () {
    return this.input !== null
  }

  /**
   * Calculates the distance between two notes in semitones.
   * The result will be positive if a is lower than b.
   *
   * @param {String} a_note - first note
   * @param {Number} a_octave - octave of the first note
   * @param {String} b_note - second note
   * @param {Number} b_octave - octave of the second note
   * @returns {Number}
   * @privater
   */
  _distanceBetweenNotes (a_note, a_octave, b_note, b_octave) {
    //noinspection JSValidateTypes
    a_note = this.notes.indexOf(a_note)
    //noinspection JSValidateTypes
    b_note = this.notes.indexOf(b_note)
    if (a_note < 0 || b_note < 0)
      throw new RangeError('invalid input given')

    let delta = b_octave - a_octave
    delta *= 12

    delta += b_note - a_note

    return delta
  }

  /**
   * Return the the note with the given distance from the given note. A positive distance will return a note of
   * higher pitch than the one given.
   *
   * Returns a note object from this.frequencies if a note object and distance are given. Returns a dummy otherwise.
   *
   * @param note
   * @param octave_or_distance
   * @param distance
   * @return {*}
   */
  noteByDistance (note, octave_or_distance, distance) {
    // if a note object is given: traverse through the links
    if (distance === undefined) {
      distance = Math.round(octave_or_distance)
      let current = note

      while (true) {
        if (distance === 0) {
          return current
        } else if (distance < 0) {
          current = current.prev
          distance++
        } else if (distance > 0) {
          current = current.next
          distance--
        } else if (current === null) {
          // element isn't in frequencies, use the default mode
          console.warn('requested note is not in frequencies and the result will miss attributes such as ' +
            'frequency and wavelength')
          octave_or_distance = note.octave
          note = note.note
          break
        }
      }
    }

    // default mode, calculate the note based on the distance and and the note + octave given
    let octave = Math.round(octave_or_distance)
    distance = Math.round(distance)

    while (Math.abs(distance) >= 12) {
      octave += Math.sign(distance)
      distance -= Math.sign(distance) * 12
    }

    let index = this.notes.indexOf(note)
    index += distance
    if (index < 0) {
      index += 12
      octave -= 1
    } else if (index >= 12) {
      index -= 12
      octave += 1
    }
    note = this.notes[index]

    return {note: note, octave: octave}
  }

  noteByFrequency (freq) {
    // calculate distance to a4 in semitones
    let distance = freq / this.a4_frequency
    distance = Math.log(distance) / Math.log(Math.pow(2, 1 / 12))

    // get the note
    let note = this.noteByDistance('a', 4, distance)

    let note_object = this.frequencies.get(note.note + String(note.octave))

    if (note_object === undefined) {
      console.warn('note requested by frequency is not in frequencies. returning a dummy (' + note.note + note.octave + ')')
      return note
    }
    return note_object
  }

  /**
   * Return the band which contains the given frequency.
   *
   * @param {Number} freq
   * @return {Number}
   * @private
   */
  _freqToBand (freq) {
    let band = Math.floor(freq / this.fft_bandwith)
    if (band < 0)
      band = 0
    else if (band >= this.analyser.frequencyBinCount)
      band = this.analyser.frequencyBinCount - 1

    return band
  }

  /**
   * Return the center frequency of the given band.
   *
   * @param {Number} band
   * @return {Number}
   * @private
   */
  _bandToFreq (band) {
    return this.fft_bandwith * band + this.fft_bandwith / 2
  }

  /**
   * Build the frequency table for note detection.
   *
   * @param {String} min_note - name of the lowest note
   * @param {Number} min_octave - octave of the lowest note
   * @param {String} max_note - name of the highest note
   * @param {Number} max_octave - octave of the highest note
   * @param {Boolean} [c_maj_only] - false if omitted, use only the c major scale (white keys)
   * @private
   */
  _buildFrequencyTable (min_note, min_octave, max_note, max_octave, c_maj_only = false) {
    // calculate distance of min and max to A4 in half steps
    let min = this._distanceBetweenNotes('a', 4, min_note, min_octave)
    let max = this._distanceBetweenNotes('a', 4, max_note, max_octave)

    // log the amount of semitones the
    console.group('generated frequency table')
    console.log('range in semitones from a4: from ' + min + ' to ' + max)

    let prev = null
    for (let i = min; i <= max; i++) {
      // get a simple note object with only the note and octave properties
      let note = this.noteByDistance('a', 4, i)

      // skip everything not c major if the parameter is true
      if (c_maj_only && note.note.length !== 1)
        continue

      // add the other properties
      note.name = note.note + String(note.octave)
      note.freq = this.a4_frequency * Math.pow(Math.pow(2, 1 / 12), i)
      note.wavelength = 1. / note.freq * this.context.sampleRate  // wavelength in samples
      if (note.wavelength > this.analyser.fftSize / 2) {
        console.warn('Can\'t support ' + name + ', it\'s wave is too long. Increase sampleRate or fftSize.')
        continue
      }

      // add the fft bands the partials will occupy
      let bands = []
      for (let i = 1; i <= 5; i++) {
        bands.push(this._freqToBand(note.freq * i))
      }
      note.bands = bands

      // add links to the previous and the next element
      note.prev = prev
      if (prev !== null)
        prev.next = note
      prev = note
      note.next = null

      // add it to the map
      this.frequencies.set(note.name, note)
    }

    // log the generated frequency table
    console.log(this.frequencies)
    console.groupEnd()
  }

  /**
   * Update the sample buffer.
   *
   * @param {boolean} [forced] - force buffer update if true, use lazy update if false or omitted
   * @private
   */
  _updateBuffer (forced = false) {
    if (!forced)
      if (this.buffer_expiration > Date.now())
        return

    this.analyser.getFloatTimeDomainData(this.buffer)
    this.buffer_expiration = Date.now() + this.buffer_decay
  }

  /**
   * Update the buffer containing the results of the FFT.
   *
   * @return {Float32Array} the current FFT buffer in dB
   * @private
   */
  _getFFTBuffer () {
    // update buffer
    let buffer = new Float32Array(this.analyser.frequencyBinCount)
    this.analyser.getFloatFrequencyData(buffer)
    return buffer
  }

  /**
   * Return a linear scale version of the given FFT buffer. Gets the current buffer if no buffer is given.
   *
   * @param {Float32Array} [buffer] - the FFT buffer to convert or (if omitted) the current buffer
   * @return {Array} the FFT buffer converted to linear
   * @private
   */
  _getFFTBufferLinear (buffer = this._getFFTBuffer()) {
    // convert to linear scale
    let linear_buffer = []
    for (let i = 0; i < buffer.length; i++)
      linear_buffer[i] = Math.pow(10, buffer[i] / 10)

    return linear_buffer
  }

  /**
   * Calculate the root mean square of all values in the sample buffer.
   *
   * @returns {number}
   */
  getRMS () {
    // use lazy update
    this._updateBuffer(false)

    let rms = 0
    for (let i = 0; i < this.buffer.length; i++) {
      rms += this.buffer[i] * this.buffer[i]
    }
    rms /= this.buffer.length
    rms = Math.sqrt(rms)

    return rms
  }

  /**
   * Takes an array of levels in dB and returns the sum of them.
   *
   * @param {Array} levels
   * @return {Number}
   * @private
   */
  static _addDeciBel (levels) {
    let total_db = 0
    for (let i = 0; i < levels.length; i++)
      total_db += Math.pow(10, levels[i] / 10)
    total_db = Math.log(total_db) / Math.log(10)
    total_db *= 10

    return total_db
  }

  /**
   * More readable version of a simple element-wise multiplication for matrices.
   *
   * @param {math.Matrix} a - base (the Matrix)
   * @param {Number} b - exponent (a scalar)
   * @return {math.Matrix}
   * @private
   */
  _matrixPow (a, b) {
    return math.map(a, x => {return Math.pow(x, b)})
  }

  /**
   * Rule out some notes that are very unlikely to be the ones played.
   *
   * @param {Float32Array} fft_buffer - the buffer used to determine possible included notes
   * @return {[Array[],String[]]}
   * @private
   */
  _getPreselection (fft_buffer) {
    // calculate the average loudness in dB
    let avg_db = 0
    for (let x of fft_buffer)
      avg_db += x
    avg_db /= fft_buffer.length

    // get every note with the peak bands (in note.bands) louder than average and build the matrix w for the NMF below
    // w consists of the parts we combine to approximate the current FFT (v) by multiplying it with the composition (h)
    let candidates = []
    for (let [name, spectrum] of this.fft_recordings) {
      let is_candidate = true
      let note = this.frequencies.get(name)
      let loudness = 0
      let viewed_bands = 0

      // eliminate notes that have at least one band below average loudness
      for (let band of note.bands) {
        if (fft_buffer[band] < avg_db) {
          is_candidate = false
          break
        } else {
          loudness += fft_buffer[band]
          viewed_bands++
        }
      }

      if (is_candidate) {
        // average the loudness on all bands
        loudness /= viewed_bands

        // add to the candidates
        candidates.push([name, spectrum, loudness])
      }
    }

    // with too many candidates: select the strongest ones
    let max_candidates = this.string_count * 2
    if (candidates.length > max_candidates) {
      candidates.sort((a, b) => b[2] - a[2])
      candidates = candidates.slice(0, max_candidates)
    }

    // convert candidates to an array of form [[keys], [spectrums]]
    let preselection = [[], []]
    for (let [k, s, l] of candidates) {
      preselection[0].push(k)
      preselection[1].push(s)
    }

    return preselection
  }

  /**
   * Do the actual calculating of the approximation returned in this.getComposition() via NMF.
   *
   * @param {String[]} key - the names of notes included in w
   * @param {Array[]} w - the spectrums of the notes to look at
   * @param {Number[]} fft_buffer_linear - the linear scale buffer whose spectrum we approximate
   * @param {Boolean} [verbose] - print out some debug infos?
   * @return {Map}
   * @private
   */
  _getComposition (key, w, fft_buffer_linear, verbose = false) {
    // NMF implemented as described in http://imtr.ircam.fr/imtr/images/Dessein2012MIG.pdf (formula 26)
    // beta-divergence as in https://hal.inria.fr/hal-00708682/file/Dessein2010ISMIR.pdf (formula 4)
    // TODO: have a look at https://pdfs.semanticscholar.org/ed4e/57411e5b1e9cf330466d66f886f8b17dea68.pdf for possible optimisations

    // convert w from the array to a matrix and transpose it
    w = math.matrix(w)
    w = math.transpose(w)

    // initialisation
    let iteration = 0
    let error = Number.POSITIVE_INFINITY
    let last_error = null
    let step_size = 1
    if (this.beta < 1)
      step_size = 1 / (2 - this.beta)
    else if (this.beta > 2)
      step_size = 1 / (this.beta - 1)

    let v = math.matrix(fft_buffer_linear)  // the signal we want to approximate
    v.resize([this.fft_cutoff_band, 1])  // force v to be a matrix instead of a vector/1D matrix and truncate it
    // w was built above
    // the composition of the parts for the approximation, init with last value if available and the key is the same
    let h = null
    if (this._last_h && this._last_key) {
      if (key.length <= this._last_key.length) {
        for (let i = 0; i < key.length; i++) {
          if (key[i] !== this._last_key[i]) {
            h = null
            break
          }
        }
      }
    }
    if (!h)
      h = math.ones(key.length, 1)

    // W (x) (v * e ^ T) ^ T  // (x) means element-wise multiplication
    let wve = math.multiply(v, math.ones(1, w.size()[1]))
    wve = math.dotMultiply(w, wve)
    wve = math.transpose(wve)

    // W * h
    let wh = math.multiply(w, h)

    do {
      iteration++

      // multiplicative update
      // (W * h) ^ (beta - 2)  // exponentiation is element-wise
      let whbeta = this._matrixPow(wh, this.beta - 2)
      let a = math.multiply(wve, whbeta)
      let b = math.dotMultiply(wh, whbeta)
      b = math.multiply(math.transpose(w), b)
      let ab = math.dotDivide(a, b)
      h = math.dotMultiply(h, this._matrixPow(ab, step_size))

      // calculate new approximation
      wh = math.multiply(w, h)

      // save the last error to compare them later
      last_error = error

      // do the first part of the error calculation
      error = this._matrixPow(wh, this.beta)
      error = math.add(error, math.dotMultiply(this.beta - 1, this._matrixPow(v, this.beta)))
      let xy = math.dotMultiply(wh, this._matrixPow(v, this.beta - 1))
      error = math.subtract(error, math.dotMultiply(this.beta, xy))

      // average the error vector
      let sum = 0
      for (let i = 0; i < error.size()[0]; i++)
        sum += error.get([i, 0])
      error = sum / error.size()[0]

      // calculate the rest of the error
      error *= 1 / (this.beta * (this.beta - 1))

      if (verbose)
        console.log(`i: ${iteration}, error: ${error}`)
    } while (error / last_error < this.error_threshold && iteration < this.max_iterations)

    // save the current h and key to speed up the next cycle
    this._last_h = h
    this._last_key = key

    // built a map with the results
    h = h.toArray()
    let composition = new Map()
    for (let i = 0; i < h.length; i++) {
      composition.set(key[i], h[i])
    }

    // print the result to the console if requested
    if (verbose) {
      console.group(`nmf results after ${iteration} iterations`)
      for (let [name, strength] of composition)
        console.log(`${name}: ${Math.round(strength * 1e9) / 1e9}`)
      console.groupEnd()
    }

    return composition
  }

  /**
   * Approximate the composition of the input signal via non-negative matrix factorisation.
   *
   * @return {Map|null}
   */
  getComposition (verbose = false) {
    // we require recordings of the fft to detect notes
    if (this.fft_recordings === null)
      return null
    if (this.fft_recordings.size < 1)
      return null

    // stop if the signal is too quiet
    let rms = this.getRMS()
    if (rms < this.min_rms) {
      this._last_h = null
      this._last_key = null
      return null
    }

    // get the fft buffer (in dB) and truncate it
    let fft_buffer = this._getFFTBuffer()
    fft_buffer = fft_buffer.slice(0, this.fft_cutoff_band)

    // preselect the notes
    let [key, w] = this._getPreselection(fft_buffer)

    // log the current candidates is verbose logging is enabled
    if (verbose) {
      let text = 'current candidates (' + key.length + '):'
      for (let x of key)
        text += ' ' + x
      console.log(text)
    }

    // convert the buffer to a linear scale
    let fft_buffer_linear = this._getFFTBufferLinear(fft_buffer)

    return this._getComposition(key, w, fft_buffer_linear, verbose)
  }

  /**
   * Calculate the active note(s) in the given composition approximated by the NMF.
   * You should deactivate note smoothing if you're not calling the function every frame.
   *
   * @param {Boolean} [smoothing] - apply smoothing to the results?
   * @param {Map} [composition] - the composition
   * @return {Map} Map containing the names of the notes
   */
  getNotes (smoothing = true, composition = this.getComposition()) {
    if (composition === null) {
      this._last_notes = []
      return new Map()
    }

    // syntax: [name, strength]
    let notes = []

    // stage 1: reduce the amount of notes with a simple threshold
    for (let [name, strength] of composition)
      if (strength > this.note_threshold)
        notes.push([name, strength])

    if (notes.length < 1) {
      this._last_notes = []
      return new Map()
    }

    //sort the notes for the rest of the stages
    notes.sort((a, b) => b[1] - a[1])

    // stage 2: reduce to only six notes
    notes = notes.slice(0, this.string_count)

    // stage 3: remove everything notably weaker than the strongest note
    for (let i = 1; i < notes.length; i++) {
      if (notes[i][1] < notes[0][1] * this.note_threshold_secondary) {
        notes = notes.slice(0, i)
        break
      }
    }

    // apply smoothing
    let reset = false
    if (smoothing) {
      // noinspection EqualityComparisonWithCoercionJS
      if (this._last_notes == false) {
        reset = true
      } else {
        // check length
        if (this._last_notes[1].length !== notes.length) {
          reset = true
        } else {
          // check notes
          for (let i = 0; i < notes.length; i++)
            if (notes[i][0] !== this._last_notes[1][i][0])
              reset = true

          // check time
          if (this._last_notes[0] > Date.now() && !reset)
            return new Map()
        }
      }

      if (reset) {
        this._last_notes = [Date.now() + this.note_smoothing_window_length, notes]
        return new Map()
      }
    }

    return new Map(notes)
  }

  // ----- FFT recording -----
  /**
   * Start recording. This only sets up everything needed for fft recording, call fft_record() to actually record.
   */
  fft_record_start () {
    // the object to save the collected data, syntax: {note.name: [spectrum]}
    this.fft_recorder_recordings = new Map()

    // the note object describing current note
    this.fft_recorder_current = this.frequencies.get('e2')

    this.fft_recorder_last_rms = 0
    this.fft_recorder_starting_time = 0

    console.log('fft recording initialised (fft sample size: ' + this.analyser.frequencyBinCount
      + ' samples, samplerate ' + this.context.sampleRate + 'Hz)')
  }

  /**
   * Returns if everything to record the fft is set up. Call fft_recording_start() if false.
   *
   * @return {Boolean} true if it's possible to record now
   */
  fft_record_is_recording () {
    if (this.fft_recorder_recordings === undefined)
      return false
    if (this.fft_recorder_current === undefined)
      return false
    if (this.fft_recorder_last_rms === undefined)
      return false
    if (this.fft_recorder_starting_time === undefined)
      return false

    return true
  }

  _fft_record_ensure_setup () {
    if (this.fft_record_is_recording())
      return true

    console.warn('Fft recording has not yet been initialized.')
    return false
  }

  /**
   * Set the note to be recorded.
   *
   * @param note
   */
  fft_record_set_note (note) {
    if (!this._fft_record_ensure_setup())
      return

    if (this.frequencies.has(note))
      this.fft_recorder_current = this.frequencies.get(note)
    else
      console.warn(note + ' is not a valid note')
  }

  /**
   * Sets the current note for fft recording to the next one. Does nothing if the next note is not in this.frequencies.
   */
  fft_record_next_note () {
    if (!this._fft_record_ensure_setup())
      return

    if (this.fft_recorder_current.next !== null)
      this.fft_recorder_current = this.fft_recorder_current.next
  }

  /**
   * Sets the current note for fft recording to the previous one. Does nothing if the previous note is not in this.frequencies.
   */
  fft_record_prev_note () {
    if (!this._fft_record_ensure_setup())
      return

    if (this.fft_recorder_current.prev !== null)
      this.fft_recorder_current = this.fft_recorder_current.prev
  }

  /**
   * Record the current fft. Has to be called repeatedly.
   */
  fft_record () {
    // check if the necessary variables have been initialised
    if (!this._fft_record_ensure_setup())
      return
    if (this.fft_recorder_current === null) {
      console.warn('the current note is note defined')
      return
    }

    // check if the signal is loud enough
    let rms = this.getRMS()
    if (rms < this.min_rms)
      return

    // check if the tone should be stable
    if (rms > this.fft_recorder_last_rms * 1.05) {
      // starting tone detected, pause recording
      this.fft_recorder_starting_time = Date.now() + 50
    }
    this.fft_recorder_last_rms = rms
    if (Date.now() < this.fft_recorder_starting_time)
      return

    // update buffer
    let fft_buffer = this._getFFTBuffer()

    // write the content of the buffer to the recordings
    let rec = this.fft_recorder_recordings.get(this.fft_recorder_current.name)
    if (rec === undefined)
      rec = []
    rec.push(fft_buffer.slice())  // copy the buffer and save it
    this.fft_recorder_recordings.set(this.fft_recorder_current.name, rec)
  }

  /**
   * Remove the recorded data for the current note
   */
  fft_record_reset_current () {
    if (!this._fft_record_ensure_setup())
      return

    this.fft_recorder_recordings.delete(this.fft_recorder_current.name)
    console.log('reset current note (' + this.fft_recorder_current.name + ')')
  }

  /**
   * Finalise the data and remove the variables used for recording.
   *
   * @param {Boolean} output_to_file - output the results in a file? false if omitted
   */
  fft_record_finalise (output_to_file = false) {
    // TODO: simplify and automate the process (for example with automatic recognition of the note)
    // TODO: accommodate for the inability to set the samplerate of the AudioContext by only recording the raw sound and doing the rest in here (for multiple samplerates)
    // ensure this.fft_recordings is a Map
    if (!this.fft_recordings)
      this.fft_recordings = new Map()

    for (let [note, recs] of this.fft_recorder_recordings) {
      // make the temporary array, its elements are automatically initialised to 0
      let fft = new Float32Array(recs[0].length)

      // average all of the recordings for this note
      for (let rec of recs)
        for (let i = 0; i < fft.length; i++)
          fft[i] += rec[i]
      for (let i = 0; i < fft.length; i++)
        fft[i] /= recs.length

      // find the max value in the averaged recording
      let max = Number.NEGATIVE_INFINITY
      for (let x of fft)
        if (x > max)
          max = x

      // calculate and apply normalisation
      let delta = this.fft_recorder_norm_peak - max
      for (let i = 0; i < fft.length; i++) {
        fft[i] += delta
      }

      // convert to linear scale
      fft = this._getFFTBufferLinear(fft)

      // write the averaged recording in the global map
      this.fft_recordings.set(note, fft.slice(0, this.fft_cutoff_band))
    }

    // trigger export
    if (output_to_file)
      this.export_fft()

    // remove the variables introduced for recording
    delete this.fft_recorder_recordings
    delete this.fft_recorder_current
    delete this.fft_recorder_last_rms
    delete this.fft_recorder_starting_time

    console.log('fft recording stopped')
  }

  /**
   * Exports the contents of this.fft_recordings to a file and prompts the user to save it.
   */
  export_fft () {
    // build the map for exporting (but in the array representation of a map)
    let export_map = []
    for (let [param, variable] of this.fft_export_mapping)
      export_map.push([param, variable])
    export_map.push(['fft', Array.from(this.fft_recordings.entries())])

    // convert the map to JSON
    let json = JSON.stringify(export_map)
    json = new Blob([json], {type: 'application/json'})

    // prompt the user to save the file
    // TODO: show this as popup on the page
    console.log('FFT recordings are being exported. Do not change the filename!')
    FileSaver.saveAs(json, `fft_recordings_${this.context.sampleRate}.json`)
  }

  /**
   * Import recordings of the fft for specific notes from a given file. The recordings are not merged with existing ones.
   * The filename is specific to the used samplerate because at least chrome an firefox do resample the signal.
   * For more details see https://bugs.chromium.org/p/chromium/issues/detail?id=432248
   *
   * @param {String} path - Path to the file containing the recordings (excluding the filename)
   */
  import_fft (path) {
    // append the filename to the path
    path += `fft_recordings_${this.context.sampleRate}.json`

    // create the request (the XHR object)
    let request = new XMLHttpRequest()
    request.open('GET', path)
    request.responseType = 'json'
    request.onreadystatechange = function () {
      if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {
        // convert the higher level Map (but not yet converting the actual recordings)
        let response = request.response
        response = new Map(response)

        // check if the imported data is usable (same version, samplerate, fft buffer size, ...)
        for (let [param, variable] of this.fft_export_mapping) {
          if (!response.has(param)) {
            console.warn(`FFT recordings could not be imported due to the parameter "${param}" missing in the file.`)
            return
          }
          if (response.get(param) !== variable) {
            console.warn(`FFT recordings could not be imported due to mismatch with the parameter "${param}."\n`
              + `local value: ${variable}, in file: ${response.get(param)}`)
            return
          }
        }

        // write the converted data to this.fft_recordings and this.fft_recordings_key
        this.fft_recordings = new Map(response.get('fft'))
        console.log(`successfully imported ${this.fft_recordings.size} entries from ${path}`)
      } else if (request.readyState === XMLHttpRequest.DONE) {
        console.warn('XHR request failed, download returned HTTP status ' + request.statusText)
      }
    }.bind(this)
    request.send()
  }
}
