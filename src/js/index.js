// Guitar trainer. A program to train navigation on a guitar.
//   Copyright (C) 2017  Tom Winter
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.

'use strict'

import 'bootstrap.native'
import 'bootstrap/dist/css/bootstrap.min.css'
import AudioProcessor from './audio_processor.js'

let audio_processor

// html elements
let e_framecounter
let e_framerate
let e_rms
let e_challenge
let e_result_table
let e_result_text

// global variables
let animation_frame_id = null
let frame_times = []
let challenge_note = null
let challenge_text = ''

setup()

function setup () {
  console.log('setup starting')

  // instantiate the audio processor
  audio_processor = new AudioProcessor()

  // load fft recordings
  audio_processor.import_fft(window.location.href + 'src/')

  // save the DOM nodes
  e_framecounter = document.getElementById('framecounter')
  e_framerate = document.getElementById('framerate')
  e_rms = document.getElementById('rms')
  e_challenge = document.getElementById('challenge')
  e_result_table = document.getElementById('result_table')
  e_result_text = document.getElementById('result_text')

  // bind events to buttons
  document.getElementById('btn_start_stop').addEventListener('click', toggle_run)
  document.getElementById('fft_rec_start_stop').addEventListener('click', fft_rec_toggle_run)
  document.getElementById('fft_rec_export').addEventListener('click', fft_rec_export)
  let fft_rec_prev = document.getElementById('fft_rec_prev')
  fft_rec_prev.addEventListener('click', audio_processor.fft_record_prev_note.bind(audio_processor))
  fft_rec_prev.addEventListener('click', fft_rec_update_current)
  let fft_rec_next = document.getElementById('fft_rec_next')
  fft_rec_next.addEventListener('click', audio_processor.fft_record_next_note.bind(audio_processor))
  fft_rec_next.addEventListener('click', fft_rec_update_current)
  document.getElementById('fft_rec_reset')
    .addEventListener('click', audio_processor.fft_record_reset_current.bind(audio_processor))
  document.getElementById('fft_rec_e2').addEventListener('click', fft_rec_set_note.bind(this, 'e2'))
  document.getElementById('fft_rec_a2').addEventListener('click', fft_rec_set_note.bind(this, 'a2'))
  document.getElementById('fft_rec_d3').addEventListener('click', fft_rec_set_note.bind(this, 'd3'))
  document.getElementById('fft_rec_g3').addEventListener('click', fft_rec_set_note.bind(this, 'g3'))
  document.getElementById('fft_rec_b3').addEventListener('click', fft_rec_set_note.bind(this, 'b3'))
  document.getElementById('fft_rec_e4').addEventListener('click', fft_rec_set_note.bind(this, 'e4'))

  // bind events to key presses
  window.addEventListener('keydown', _handle_keydown)

  // misc
  fft_rec_update_current()

  animation_frame_id = window.requestAnimationFrame(update)

  console.log(`setup finished (samplerate: ${audio_processor.context.sampleRate})`)

}

function update (timestamp) {
  // increment framecounter
  e_framecounter.textContent = Number.parseInt(e_framecounter.textContent) + 1

  // calculate framerate
  frame_times.push(timestamp)
  if (frame_times.length > 5)
    frame_times.shift()
  if (frame_times.length >= 2) {
    // calculate average time between frames
    let avg = 0
    for (let i = 1; i < frame_times.length; i++)
      avg += frame_times[i] - frame_times[i - 1]
    avg /= frame_times.length - 1

    // convert to frames per second and print
    let fps = 1000 / avg
    e_framerate.textContent = Math.round(fps * 1000) / 1000
  } else {
    e_framerate.textContent = 'not enough data'
  }

  if (audio_processor.available) {
    e_rms.textContent = Math.round(audio_processor.getRMS() * 1e5) / 1e5
    let composition = audio_processor.getComposition(true)
    let display_notes = ['e2', 'a2', 'd3', 'g3', 'b3', 'e4']
    if (composition !== null) {
      for (let i = 0; i < e_result_table.children[1].children.length; i++) {
        //get the current row
        let row = e_result_table.children[1].children[i]
        // write the name of the note
        row.children[0].textContent = display_notes[i]
        // write the strength of the note
        let row_note_strength = 0
        if (composition.has(display_notes[i]))
          composition.get(display_notes[i])
        row.children[1].textContent = Math.round(row_note_strength * 1e5) / 1e5
      }
      let notes = audio_processor.getNotes(true, composition)
      let notes_text = ''
      for (let x of notes)
        notes_text += x[0] + ' '
      e_result_text.textContent = notes_text
    }
    // TODO: display challenge

    if (audio_processor.fft_record_is_recording())
      audio_processor.fft_record()
  }

  animation_frame_id = window.requestAnimationFrame(update)
}

function _handle_keydown (e) {
  switch (e.code) {
    case 'Escape':
      toggle_run()
      break
    case 'Enter':
      fft_rec_toggle_run()
      break
    case 'ArrowLeft':
      audio_processor.fft_record_prev_note()
      fft_rec_update_current()
      break
    case 'ArrowRight':
      audio_processor.fft_record_next_note()
      fft_rec_update_current()
      break
  }
}

function toggle_run () {
  let btn = document.getElementById('btn_start_stop')

  if (animation_frame_id === null) {
    // start
    animation_frame_id = window.requestAnimationFrame(update)
    btn.textContent = 'stop'
  } else {
    // stop
    window.cancelAnimationFrame(animation_frame_id)
    animation_frame_id = null
    btn.textContent = 'run'
  }
}

function fft_rec_update_current () {
  let node = document.getElementById('fft_rec_current')
  let note = audio_processor.fft_recorder_current
  if (note === undefined)
    node.textContent = 'n/a'
  else
    node.textContent = note.name
}

function fft_rec_toggle_run () {
  if (audio_processor.fft_record_is_recording()) {
    audio_processor.fft_record_finalise()
    document.getElementById('fft_rec_start_stop').textContent = 'Start'
    document.getElementById('fft_rec_prev').disabled = true
    document.getElementById('fft_rec_next').disabled = true
    document.getElementById('fft_rec_reset').disabled = true
    document.getElementById('fft_rec_e2').disabled = true
    document.getElementById('fft_rec_a2').disabled = true
    document.getElementById('fft_rec_d3').disabled = true
    document.getElementById('fft_rec_g3').disabled = true
    document.getElementById('fft_rec_b3').disabled = true
    document.getElementById('fft_rec_e4').disabled = true
  } else {
    audio_processor.fft_record_start()
    document.getElementById('fft_rec_start_stop').textContent = 'Stop'
    document.getElementById('fft_rec_prev').disabled = false
    document.getElementById('fft_rec_next').disabled = false
    document.getElementById('fft_rec_reset').disabled = false
    document.getElementById('fft_rec_e2').disabled = false
    document.getElementById('fft_rec_a2').disabled = false
    document.getElementById('fft_rec_d3').disabled = false
    document.getElementById('fft_rec_g3').disabled = false
    document.getElementById('fft_rec_b3').disabled = false
    document.getElementById('fft_rec_e4').disabled = false
  }
  fft_rec_update_current()
}

function fft_rec_set_note (note) {
  audio_processor.fft_record_set_note(note)
  fft_rec_update_current()
}

function fft_rec_export () {
  if (audio_processor.fft_record_is_recording())
    fft_rec_toggle_run()

  audio_processor.export_fft()
}

// XXX: rewrite this to function without p5
function newChallenge () {
  let limit = Math.floor(sketch.random() * audio_processor.frequencies.size)
  let notes_iterator = audio_processor.frequencies.keys()
  for (let i = 0; i <= limit; i++)
    challenge_note = notes_iterator.next().value
  challenge_text = 'Play ' + challenge_note
}

/*
// XXX: rewrite this to function without p5
function drawFFT (xmin, ymin, xmax, ymax, fft, highlighted) {
  // a frequency to highlight in the fft
  if (highlighted)
    highlighted = audio_processor._freqToBand(highlighted)

  let limit = audio_processor._freqToBand(10000)
  if (fft.length < limit)
    limit = fft.length
  for (let i = 0; i < limit - 1; i++) {
    let x1 = sketch.map(i, 0, limit, xmin, xmax)
    let y1 = sketch.map(fft[i], audio_processor.noise_threshold, 0, ymin, ymax)
    let x2 = sketch.map(i + 1, 0, limit, xmin, xmax)
    let y2 = sketch.map(fft[i + 1], audio_processor.noise_threshold, 0, ymin, ymax)
    sketch.line(x1, y1, x2, y2)

    if (i === highlighted) {
      let x = sketch.map(i, 0, limit, xmin, xmax)
      sketch.line(x, ymin, x, ymax)
    }
  }
  sketch.line(xmin, ymin, xmax, ymin)
  sketch.line(xmax, ymin, xmax, ymax)
  sketch.line(xmin, ymax, xmax, ymax)
  sketch.line(xmin, ymin, xmin, ymax)
}
*/
