# GUITAR TRAINER

A program intended to train fretboard navigation, intervals, harmonics
and chords on guitar.

**This project has been deprecated in favor of a complete rewrite.
You can find the new version at [https://gitlab.com/twinter/guitar-trainer2].**

It currently is a (more or less) working prototype.

## Usage
- you need a modern browser (tested and developed in chrome but firefox should work just as good)
- detection should be better if you use an electric guitar and an audio interface
- just open index.html

## Development
- you need [node](https://nodejs.org/en/).
- you need a web server ([light-server](https://yarnpkg.com/en/package/light-server) is awesome if you don't already have one).
- the package management is done with [yarn](https://yarnpkg.com).
- deployment is done with [webpack](https://webpack.js.org).

## ToDo
- do the note recognition async with webworkers
- add more features (like training intervals, chords, progressions of notes, etc.)

## License (GPL-3.0+)
Copyright (C) 2017  Tom Winter

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## external Libraries (integrated in the file loaded by the user)
- [Apache-2.0] https://yarnpkg.com/en/package/mathjs
- [MIT] https://yarnpkg.com/en/package/file-saver
- [MIT] https://yarnpkg.com/en/package/bootstrap
- [MIT] https://yarnpkg.com/en/package/bootstrap.native
